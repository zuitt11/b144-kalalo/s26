let http = require("http");
//require() is a directive method used to load node.js modules
//module- software component/part of a program that contains one or mmore routines.
	//http module- hypertext transfer protocol; lets node.js transfer data; a set of individual files that contain code to create a "component" that helps establish data transfer between applications

http.createServer(function(request, response){
//createServer - allows creation of http server that listens to requests on a specified port and gives response back to the client; accepts a function and allows performing of a task for the server.
	response.writeHead(200, {'Content-Type': 'text/plain'});
	response.end ('Hello World');
}).listen(8080); //default: http://localhost:4000


console.log('Server running at localhost:8080')
/*
	port- virtual point where network connections start and end; each port is associated with a specific process/server
	
	server will be assigned to port 4000 via ".listen(4000)" method where the server will listen to any requests that are sent to our server
	
	use .writeHead() method to set a status code for the response; 200 is the usual status

	content-type - sets the content-type as a plain text

	response.end () - end the response process

	console.log () - determines whether the server is running or not
*/




