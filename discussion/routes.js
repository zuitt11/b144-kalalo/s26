const http = require("http")

//varible to store the port number
const port = 4000


//variable server that stores the out put of the create server


//with conditions
const server = http.createServer((req, res)=>{
	//http:localhost:4000/greeting
	if(req.url == '/greeting'){
		res.writeHead(200, {'Content-Type': 'text/plain'})
		res.end ('Hello Again')
	}
	//http:localhost:4000/homepage
	else if (req.url == '/homepage'){
		res.writeHead (200, {'Content-Type': 'text/plain'});
		res.end('This is the homepage with a 200 status')
	}
		//http:localhost:4000/<other-routes>
	else{
		res.writeHead (404, {'Content-Type': 'text/plain'});
		res.end('Page not found')
	}

})

//use the server and port server created
server.listen(port)

//when the server is running, console will print...
console.log(`server now accessible at localhost:${port}`)


